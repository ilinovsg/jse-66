package ru.ilinovsg.tm.client.repository.impl;

import org.mapstruct.factory.Mappers;
import ru.ilinovsg.tm.client.exception.ObjectNotFoundException;
import ru.ilinovsg.tm.client.generated.*;
import ru.ilinovsg.tm.client.model.User;
import ru.ilinovsg.tm.client.repository.UserSoapRepository;
import ru.ilinovsg.tm.client.repository.mapper.UserMapper;

import java.util.List;

public class UserSoapRepositoryImpl implements UserSoapRepository {
    private final UserControllerImplService service;
    private final UserController controllerImplPort;
    UserMapper userMapper;

    public UserSoapRepositoryImpl() {
        service = new UserControllerImplService();
        controllerImplPort = service.getUserControllerImplPort();
        userMapper = Mappers.getMapper(UserMapper.class);
    }

    public UserSoapRepositoryImpl(UserControllerImplService service, UserController controllerImplPort, UserMapper userMapper) {
        this.service = service;
        this.controllerImplPort = controllerImplPort;
        this.userMapper = userMapper;
    }

    @Override
    public Integer createUser(User user) {
        UserDTO userDTO = userMapper.userToUserDTO(user);
        UserResponseDTO userResponseDTO = controllerImplPort.createUser(userDTO);
        if (userResponseDTO.getStatus() == Status.OK) {
            return Math.toIntExact(userResponseDTO.getPayload().getId());
        }
        return -1;
    }

    @Override
    public Integer updateUser(User user) {
        UserDTO userDTO = userMapper.userToUserDTO(user);
        UserResponseDTO userResponseDTO = controllerImplPort.updateUser(userDTO);
        if (userResponseDTO.getStatus() == Status.OK) {
            return 0;
        }
        return -1;
    }

    @Override
    public User findById(Long id) {
        UserResponseDTO userResponseDTO = controllerImplPort.getUser(id);
        if (userResponseDTO.getStatus() == Status.OK) {
            return userMapper.userDTOToUser(userResponseDTO.getPayload());
        }
        throw new ObjectNotFoundException();
    }

    @Override
    public void delete(Long id) {
        controllerImplPort.deleteUser(id);
    }

    @Override
    public List<User> findAll() {
        UserListResponseDTO userListResponseDTO = controllerImplPort.getAllUsers();
        return userMapper.userListDTOToUser(userListResponseDTO.getPayload());
    }
}
