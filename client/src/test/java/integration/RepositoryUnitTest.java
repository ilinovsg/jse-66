package integration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.ilinovsg.tm.client.generated.*;
import ru.ilinovsg.tm.client.model.User;
import ru.ilinovsg.tm.client.repository.UserSoapRepository;
import ru.ilinovsg.tm.client.repository.impl.UserSoapRepositoryImpl;
import ru.ilinovsg.tm.client.repository.mapper.UserMapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class RepositoryUnitTest {
    private static final Long USER_ID = 1L;
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String FIRSTNAME = "firstName";
    private static final String LASTNAME = "lastName";
    private static final Role ROLE = Role.USER;
    UserMapper userMapper;
    private UserControllerImplService service;
    private UserController controllerImplPort;
    private UserSoapRepository userSoapRepository;

    @BeforeEach
    void setUp() {
        service = Mockito.mock(UserControllerImplService.class);
        controllerImplPort = Mockito.mock(UserController.class);
        userMapper = Mockito.mock(UserMapper.class);
        userSoapRepository = new UserSoapRepositoryImpl(service, controllerImplPort, userMapper);
    }

    @Test
    void testCreateUser() {
        User user = User.builder().login(LOGIN).password(PASSWORD).firstName(FIRSTNAME).lastName(LASTNAME).role(ROLE).build();
        UserResponseDTO userResponseDTO = new UserResponseDTO();
        UserDTO userDTO = new UserDTO();
        userDTO.setId(USER_ID);
        userResponseDTO.setPayload(userDTO);
        userResponseDTO.setStatus(Status.OK);
        when(controllerImplPort.createUser(any())).thenReturn(userResponseDTO);
        int id = userSoapRepository.createUser(user);
        assertEquals(USER_ID, id);
    }
}
