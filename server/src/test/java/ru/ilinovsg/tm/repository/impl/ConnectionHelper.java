package ru.ilinovsg.tm.repository.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionHelper {

    private ConnectionHelper() {

    }

    public static Connection getH2Connection() {
        try (InputStream input = new FileInputStream("src/test/resources/jdbc.properties")) {
            Properties properties = new Properties();
            properties.load(input);
            return DriverManager.getConnection(properties.getProperty("jdbc.url"),
                    properties.getProperty("jdbc.username"),
                    properties.getProperty("jdbc.password"));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
