package ru.ilinovsg.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import ru.ilinovsg.tm.entity.User;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDTO implements Serializable {
    public static final Long serialVersionUID = 1L;

    private Long id;
    private String name = "";
    private String description = "";
    //private Long userId;
    private User user;
}
