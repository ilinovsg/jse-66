package ru.ilinovsg.tm.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.entity.Project;

import java.util.List;
import java.util.Optional;

@Repository
public class ProjectRepositoryImpl implements ProjectRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public Optional<Project> create(Project input) {
        em.merge(input);
        return Optional.of(input);
    }

    @Override
    @Transactional
    public Optional<Project> update(Project input) {
        em.merge(input);
        return Optional.of(input);
    }

    @Override
    public Optional<Project> findById(Long id) {
        Project project = em.find(Project.class, id);
        return Optional.of(project);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Project project = em.find(Project.class, id);
        em.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return em.createQuery("SELECT c from " + Project.class.getName() + " c", Project.class)
                .getResultList();
    }
}
