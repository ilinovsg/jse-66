package ru.ilinovsg.tm.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.ilinovsg.tm.config.HibernateConfig;

import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.entity.Task;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public Optional<Task> create(Task input) {
        em.merge(input);
        return Optional.of(input);
    }

    @Override
    @Transactional
    public Optional<Task> update(Task input) {
        em.merge(input);
        return Optional.of(input);
    }

    @Override
    public Optional<Task> findById(Long id) {
        Task task = em.find(Task.class, id);
        return Optional.of(task);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Task task = em.find(Task.class, id);
        em.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return em.createQuery("SELECT c from " + Task.class.getName() + " c", Task.class)
                .getResultList();
    }
}
