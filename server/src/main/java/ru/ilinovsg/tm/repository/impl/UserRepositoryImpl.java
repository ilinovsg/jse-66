package ru.ilinovsg.tm.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.ilinovsg.tm.config.HibernateConfig;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.repository.UserRepository;
import ru.ilinovsg.tm.entity.User;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public Optional<User> create(User input) {
        em.merge(input);
        return Optional.of(input);
    }

    @Override
    @Transactional
    public Optional<User> update(User input) {
        em.merge(input);
        return Optional.of(input);
    }

    @Override
    public Optional<User> findById(Long id) {
        User user = em.find(User.class, id);
        return Optional.of(user);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        User user = em.find(User.class, id);
        em.remove(user);
    }

    @Override
    public List<User> findAll() {
        return em.createQuery("SELECT c from " + User.class.getName() + " c", User.class)
                .getResultList();
    }
}
