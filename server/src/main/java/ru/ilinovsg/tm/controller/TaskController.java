package ru.ilinovsg.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ru.ilinovsg.tm.dto.*;
import ru.ilinovsg.tm.service.impl.TaskServiceImpl;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TaskServiceImpl taskServiceImpl;

    @GetMapping("/{id}/")
    public TaskResponseDTO getTaskById(@PathVariable Long id) {
        return taskServiceImpl.getTask(id);
    }

    @GetMapping(value = "/", produces = "application/json")
    public TaskListResponseDTO getAllTasks() {
        return taskServiceImpl.getAllTasks();
    }

    @GetMapping("/findByName")
    public TaskListResponseDTO getTaskByName(@RequestParam(required = false) String name, @RequestParam(required = false) String email) {
        return taskServiceImpl.findByName(name);
    }

    @PostMapping("/")
    public TaskResponseDTO createTask(@RequestBody TaskDTO taskDTO) {
        return taskServiceImpl.createTask(taskDTO);
    }

    @DeleteMapping("/{id}/")
    public TaskResponseDTO deleteTask(@PathVariable Long id) {
        return taskServiceImpl.deleteTask(id);
    }

    @PatchMapping("/")
    public TaskResponseDTO updateTask(@RequestBody TaskDTO taskDTO) {
        return taskServiceImpl.updateTask(taskDTO);
    }
}
