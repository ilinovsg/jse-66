package ru.ilinovsg.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ru.ilinovsg.tm.dto.*;
import ru.ilinovsg.tm.service.impl.ProjectServiceImpl;

@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private ProjectServiceImpl projectServiceImpl;

    @GetMapping("/{id}/")
    public ProjectResponseDTO getProjectById(@PathVariable Long id) {
        return projectServiceImpl.getProject(id);
    }

    @GetMapping(value = "/", produces = "application/json")
    public ProjectListResponseDTO getAllTasks() {
        return projectServiceImpl.getAllProjects();
    }

    @GetMapping("/findByName")
    public ProjectListResponseDTO getTaskByName(@RequestParam(required = false) String name, @RequestParam(required = false) String email) {
        return projectServiceImpl.findByName(name);
    }

    @PostMapping("/")
    public ProjectResponseDTO createTask(@RequestBody ProjectDTO projectDTO) {
        return projectServiceImpl.createProject(projectDTO);
    }

    @DeleteMapping("/{id}/")
    public ProjectResponseDTO deleteTask(@PathVariable Long id) {
        return projectServiceImpl.deleteProject(id);
    }

    @PatchMapping("/")
    public ProjectResponseDTO updateTask(@RequestBody ProjectDTO projectDTO) {
        return projectServiceImpl.updateProject(projectDTO);
    }
}
