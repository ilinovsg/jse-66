package ru.ilinovsg.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ilinovsg.tm.dto.UserDTO;
import ru.ilinovsg.tm.dto.UserListResponseDTO;
import ru.ilinovsg.tm.dto.UserResponseDTO;
import ru.ilinovsg.tm.service.impl.UserServiceImpl;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @GetMapping("/{id}/")
    public UserResponseDTO getUserById(@PathVariable Long id) {
        return userServiceImpl.getUser(id);
    }

    @GetMapping(value = "/", produces = "application/json")
    public UserListResponseDTO getAllUsers() {
        return userServiceImpl.getAllUsers();
    }

    @GetMapping("/findByName")
    public UserListResponseDTO getUserByName(@RequestParam(required = false) String name, @RequestParam(required = false) String email) {
        return userServiceImpl.findByName(name);
    }

    @PostMapping("/")
    public UserResponseDTO createUser(@RequestBody UserDTO userDTO) {
        return userServiceImpl.createUser(userDTO);
    }

    @DeleteMapping("/{id}/")
    public UserResponseDTO deleteUser(@PathVariable Long id) {
        return userServiceImpl.deleteUser(id);
    }

    @PatchMapping("/")
    public UserResponseDTO updateUser(@RequestBody UserDTO userDTO) {
        return userServiceImpl.updateUser(userDTO);
    }
}
