package ru.ilinovsg.tm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ilinovsg.tm.service.UserService;
import ru.ilinovsg.tm.dto.UserDTO;
import ru.ilinovsg.tm.dto.UserListResponseDTO;
import ru.ilinovsg.tm.dto.UserResponseDTO;
import ru.ilinovsg.tm.entity.User;
import ru.ilinovsg.tm.enumerated.Status;
import ru.ilinovsg.tm.mapper.UserMapper;
import ru.ilinovsg.tm.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserResponseDTO createUser(UserDTO userDTO) {
        User user = User.builder()
                .login(userDTO.getLogin())
                .password(userDTO.getPassword())
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .build();
        Optional<User> userOptional = userRepository.create(user);
        if (userOptional.isPresent()) {
            return UserResponseDTO.builder().payload(UserMapper.toDto(userOptional.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO updateUser(UserDTO userDTO) {
        User user = User.builder()
                .id(userDTO.getId())
                .login(userDTO.getLogin())
                .password(userDTO.getPassword())
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .build();
        Optional<User> userOptional = userRepository.update(user);
        if (userOptional.isPresent()) {
            return UserResponseDTO.builder().payload(UserMapper.toDto(userOptional.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO deleteUser(Long id) {
        userRepository.delete(id);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO getUser(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            return UserResponseDTO.builder().payload(UserMapper.toDto(userOptional.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserListResponseDTO getAllUsers() {
        List<User> users = userRepository.findAll();
        UserDTO[] usersArray = users.stream().map(user -> UserMapper.toDto(user)).toArray(UserDTO[]::new);
        return UserListResponseDTO.builder().status(Status.OK).payload(usersArray).build();
    }

    @Override
    public UserListResponseDTO findByName(String name) {
        List<User> users = userRepository.findAll().stream().filter(
                user -> name.equals(user.getLogin()))
                .collect(Collectors.toList());
        UserDTO[] usersArray = users.stream().map(user -> UserMapper.toDto(user)).toArray(UserDTO[]::new);
        return UserListResponseDTO.builder().status(Status.OK).payload(usersArray).build();
    }
}
