package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.dto.UserDTO;
import ru.ilinovsg.tm.dto.UserListResponseDTO;
import ru.ilinovsg.tm.dto.UserResponseDTO;

public interface UserService {
    UserResponseDTO createUser(UserDTO userDTO);

    UserResponseDTO updateUser(UserDTO userDTO);

    UserResponseDTO deleteUser(Long id);

    UserResponseDTO getUser(Long id);

    UserListResponseDTO getAllUsers();

    UserListResponseDTO findByName(String name);
}
